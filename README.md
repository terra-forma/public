# Terra Forma

## Concevoir et tester l'observatoire intelligent des territoires à l'ère de l'Anthropocène.

![Terra Forma](images/logo.png)

Le projet PIA3 EQUIPEX + TERRA FORMA vise à concevoir et tester des observatoires in-situ des territoires, apportant une nouvelle vision multi-messagers, couplant les points de vue des capteurs sur les dynamiques humaine, biotique et abiotique.

L'Anthropocène, nouvelle période géologique dans laquelle les actions humaines modifient l'habitabilité de la Terre pour toutes les formes de vie, pose de nouveaux défis scientifiques que des approches fragmentées ne peuvent résoudre, en raison des interactions complexes entre les humains, les vivants et les non-vivants. Le développement d’une approche holistique à une échelle pertinente pour une recherche et une action territorialisées passe par le développement de capteurs nouveaux et intelligents. C’est l’objet de Terra Forma que de développer des capteurs de l’eau, de sols, de l’air, des humains et des vivants pour « terra former » les territoires de l’Anthropocène. Terra Forma répond aussi bien à des questions scientifiques fondamentales qu’à des demandes des porteurs d’enjeux des territoires concernant le capital sol, les ressources en eau, la biodiversité et l'intégrité des paysages.

TERRA FORMA, porté par les infrastructures de recherche OZCAR ([Observatoires de la Zone Critique : Application et Recherche[(https://www.ozcar-ri.org/fr/ozcar-observatoires-de-la-zone-critique-applications-et-recherche/)]) et RZA ([Réseau des Zones Ateliers](http://www.za-inee.org/)), rassemble une centaine de chercheurs et d'ingénieurs de toute la France. Il repose grandement sur la co-construction avec les divers utilisateurs et les citoyens : aussi bien du point de vue de la conception, que de la réalisation, de l'utilisation, de la formation, de la communication etc..

TERRA FORMA propose de développer et déployer des réseaux de capteurs intelligents, ce qui implique l'adaptation de nouvelles technologies pour l'environnement et la production massive de données hétériogènes. Ce réseau de capteurs intelligents questionne en particulier l'habitabilité de la Terre à l'heure de l'Anthropocène.

TERRA FORMA repose entièrement sur le principe de la recherche/action et propose la construction d'une infrastructure sociale qui s'appuie sur les développements instrumentaux.

# Contact
* [Laurent Longuevergne](https://geosciences.univ-rennes1.fr/interlocuteurs/laurent-longuevergne) (CNRS, Géosciences Rennes)
* [Arnaud Elger](https://www.eco.omp.eu/author/arnaud-elger/) (Université Toulouse III, ECOLAB/OMP) 
* [Virginie Girard](https://www.linkedin.com/in/virginie-girard-9a92418) (Université Grenoble Alpes, CNRS, eLTER France)

# Partenaires
* Laboratoires impliqués : CARRTEL, CEBC, CEFE, Centre de Géosciences, CERFE, CESBIO, Chrono-environnement, CRAL, CReSTIC, DT-INSU, Dynafor, ECOBIO, ECOLAB, EVS, GET, GR, GSMA, HABITER UR, IGE, IM2NP, IPAG, IPGP, IRISA, IRIT, ISM, ISTO, LAAS, LECA, LEMAR, LHYGES, LIG, LIRMM, LMGE, LPC, LRPG, LSIS, RiverLy, SAS
* CNRS : INSU, INEE, INSIS, IN2P3, INP, INS2I, INSHS, INSB
* Autres organismes de recherche : IRD, INRAE, IPGP
* Ecole d’ingénieur : Mines ParisTech
* Universités : Grenoble, Savoie-Mont-Blanc, Toulouse, Rennes, Clermont-Auvergne, Montpellier, Reims, Toulon, Franche Comté, Orléans, Strasbourg, Aix Marseille
* EPIC : INERIS
* PME : Extralab
* OZCAR ([Observatoires de la Zone Critique : Application et Recherche](https://www.ozcar-ri.org/fr/ozcar-observatoires-de-la-zone-critique-applications-et-recherche/)])
* RZA ([Réseau des Zones Ateliers](http://www.za-inee.org/))



# Financeur
[ANR](https://anr.fr/), [PIA3 EQUIPEX +](https://anr.fr/fr/detail/call/appel-a-manifestations-dinteret-equipements-structurants-pour-la-recherche-esr-equipex/)
